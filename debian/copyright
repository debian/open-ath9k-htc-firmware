Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: The firmware for QCA AR7010/AR9271 802.11n USB NICs
Upstream-Contact: ath9k_htc_fw@lists.infradead.org
Source: https://github.com/qca/open-ath9k-htc-firmware.git
Files-Excluded: sboot/* docs/* Makefile
Comment: Directories sboot/ and docs/ of upstream repository contain binary
 objects that are useful for reference and in-depth debugging purposes only
 and are not needed to generate a firmware image, hence they're removed
 during repacking to provide a DFSG-compliant tarball of original sources.
 The top-level Makefile causes non-DFSG sources to be downloaded and
 is also removed to not be a source of additional confusion.
 .
 The tarballs are repacked and made with uscan according to the
 debian/watch file.
 .
 The NOTICE.txt affirms that some separately-licensed components
 are subject to Qualcomm's BSD-3-Clause-Clear when used in combination:
 .
 > This NOTICE.TXT file contains certain notices of software components included
 > with the software that QUALCOMM ATHEROS Incorporated ('Qualcomm Atheros') is
 > required to provide you. Notwithstanding anything in the notices in this file,
 > your use of these software components together with the Qualcomm Atheros
 > software (Qualcomm Atheros software hereinafter referred to as 'Software') is
 > subject to the terms of your license from Qualcomm Atheros.
 .
 The following copyright and license statements reflect notices directly in the
 files which reflects the terms for possible disjoint use.

Files: *
Copyright: 1991, 1993, The Regents of the University of California.
 2002-2004, Sam Leffler, Errno Consulting
 2013, Qualcomm Atheros, Inc.
 2016, Oleksij Rempel <linux@rempel-privat.de>
License: BSD-3-Clause-Clear

Files: debian/*
Copyright: 2016, Aurelien Jarno <aurelien@aurel32.net>
 2016, Paul Fertser <fercerpav@gmail.com>
 2016, Oleksij Rempel <linux@rempel-privat.de>
 2020-2021, John Scott <jscott@posteo.net>
License: GPL-2+

Files: debian/firmware-ath9k-htc.metainfo.xml
Copyright: 2016, Oleksih Rempel <linux@rempel-privat.de>
License: Expat

Files: .editorconfig
 .travis.yml
Copyright: 2016, Nicola Spanti (RyDroid)
License: FSFAP-with-endorsements-clause

Files: target_firmware/magpie_fw_dev/target/inc/xtensa-elf/*
 target_firmware/magpie_fw_dev/target/inc/xtensa/*
Copyright: 2013, Tensilica Inc.
License: Expat

Files: target_firmware/magpie_fw_dev/target/inc/asf_queue.h
Copyright: 1991, 1993 The Regents of the University of California
License: BSD-4-Clause

Files: target_firmware/wlan/ieee80211.h
 target_firmware/wlan/_ieee80211.h
Copyright: 2004 Atheros Communications, Inc.
 2001, Atsushi Onoe
 2002-2005, Sam Leffler, Errno Consulting
 2013, Qualcomm Atheros, Inc.
License: BSD-3-Clause or GPL-2

Files: target_firmware/wlan/if_ath_pci.?
 target_firmware/wlan/if_athrate.h
Copyright: 2002-2004, Sam Leffler, Errno Consulting
 2004, Atheros Communications, Inc.
 2004, Video54 Technologies, Inc.
 2013, Qualcomm Atheros, Inc.
License: BSD-3-Clause-Clear or GPL-2

Files: target_firmware/wlan/if_llc.h
Copyright: 1988, 1993, The Regents of the University of California
 2002-2004, Sam Leffler, Errno Consulting
 2013, Qualcomm Atheros, Inc.
License: BSD-3-Clause-Clear or GPL-2, and BSD-4-Clause

Files: scripts/*
Copyright: 2001, Dave Jones
  2005, Joel Schopp <jschopp@austin.ibm.com>
  2007-2008, Andy Whitcroft <apw@uk.ibm.com>
  2008-2010, Andy Whitcroft <apw@canonical.com>
License: GPL-2

Files: target_firmware/magpie_fw_dev/target/cmnos/k2_fw_cmnos_printf.c
Copyright: 1998-2002, Red Hat, Inc.
 2002, Gary Thomas
Comment: NOTICE.txt specifies that as long as this is used in combination
 with Qualcomm-licensed code, this file may be treated as subject to that
 license (BSD-3-Clause-Clear) as well.
License: GPL-2+-with-linking-exception
 eCos is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free
 Software Foundation; either version 2 or (at your option) any later version.
 .
 eCos is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 As a special exception, if other files instantiate templates or use macros
 or inline functions from this file, or you compile this file and link it
 with other works to produce a work based on this file, this file does not
 by itself cause the resulting work to be covered by the GNU General Public
 License. However the source code for this file must still be made available
 in accordance with section (3) of the GNU General Public License.
 .
 This exception does not invalidate any other reasons why a work based on
 this file might be covered by the GNU General Public License.
 . 
 Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
 at http://sources.redhat.com/ecos/ecos-license/
 . 
 On Debian systems, the text of the GNU General Public License, version 2
 is available at /usr/share/common-licenses/GPL-2

License: BSD-3-Clause-Clear
 Redistribution and use in source and binary forms, with or without
 modification, are permitted (subject to the limitations in the
 disclaimer below) provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the
   distribution.
 .
 * Neither the name of Qualcomm Atheros nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.
 .
 NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
 GRANTED BY THIS LICENSE.  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
 HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-4-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 4. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: FSFAP-with-endorsements-clause
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided this notice is
 preserved.  This file is offered as-is, without any warranty.
 Names of contributors must not be used to endorse or promote products
 derived from this file without specific prior written permission.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License, version 2 as
 published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the text of the GNU General Public License version 2
 is available at /usr/share/common-licenses/GPL-2

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the text of the GNU General Public License, version 2
 can be found at /usr/share/common-licenses/GPL-2
